const fs = require('fs');

exports.read = function (fileURL, cb) {

	const releases = [];

	fs.readFile(fileURL, 'utf8', (err, data) => {

		if (err) {
			throw err;
		}

		let release = new Release();

		let strings = data.split('\n').map(item => item.trim())
		let type;

		for (let i = 0; i < strings.length; i++) {
			const string = strings[i];
			let splitString = string.split(' ');
			const section =
				splitString[0] === '##' ?
				'release' :
				splitString[0] === '###' ?
				'type' :
				splitString[0] === '-' ?
				'change' : 'blank';


			if (section === 'release') {
				const version = splitString[1].replace(/[\[\]]/g, '');
				splitString.shift()
				splitString.shift()
				const title = splitString.join(' ')
				
				release.title = title;
				release.version = version;
			}

			if (section === 'type') {
				type = splitString[1].toLowerCase();
			}

			if (section === 'change') {
				
				splitString.shift()

				splitString = splitString.join(' ')

				release.push(type, splitString)
			}

			if (section === 'blank' || strings.length === i + 1) {
				releases.push(release)

				if (strings.length === i + 1)
					release = new Release();
			}

		}


		if (cb)
			return cb(releases)
		else
			console.error(new Error('no callback provided'))

	})

	function Release() {
		this.title;
		this.version;
		this.added = []
		this.changed = []
		this.removed = []

		this.push = (type, data) => {
			this[type].push(data)
		}
	}
}